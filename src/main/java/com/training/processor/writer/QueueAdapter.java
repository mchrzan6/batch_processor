package com.training.processor.writer;


import com.training.processor.ProcessorConfiguration;
import com.training.processor.model.City;
import com.training.processor.model.CityWithAbbreviation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class QueueAdapter implements ItemWriter<CityWithAbbreviation> {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public QueueAdapter(RabbitTemplate rabbitTemplate) {
        super();
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void write(List<? extends CityWithAbbreviation> list) {
        list.forEach(this::send);
    }

    /**
     * Sends an object to "test-queue" queue.
     *
     * @param object - a message to be sent
     * @param <T>    - class extending City.class
     */
    private <T extends City> void send(T object) {
        log.info(String.format("Sending: %s", object));
        rabbitTemplate.convertAndSend(ProcessorConfiguration.TEST_QUEUE_NAME, object);
    }
}
