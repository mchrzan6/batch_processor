package com.training.processor.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class AbbreviationDto {
    private String city;
    private String abbreviation;
}
