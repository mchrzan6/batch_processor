package com.training.processor.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class City implements Serializable {
    private String cityName;
    private String county;
    private String temperature;
}
