package com.training.processor.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class CityWithAbbreviation extends City {
    private String abbreviation;

    public CityWithAbbreviation(String city, String county, String temperature, String abbreviation) {
        super(city, county, temperature);
        this.abbreviation = abbreviation;
    }

    @Override
    public String toString() {
        return "CityWithAbbreviation{" +
                "abbreviation='" + abbreviation + '\'' + super.toString() + '\'' +
                '}';
    }
}
