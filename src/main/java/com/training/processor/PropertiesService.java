package com.training.processor;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
public class PropertiesService {
    @Getter
    private final String csvFilePath;

    @Getter
    private final String gatewayAddress;

    @Autowired
    public PropertiesService(@Value("${app.csv.path}") String csvFilePath,
                             @Value("${app.gateway.address}") String gatewayAddress) {
        this.csvFilePath = csvFilePath;
        this.gatewayAddress = gatewayAddress;
    }

    public Resource getCsvResource() {
        return new ClassPathResource(csvFilePath);
    }
}
