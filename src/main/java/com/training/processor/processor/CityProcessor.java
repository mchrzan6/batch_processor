package com.training.processor.processor;

import com.training.processor.PropertiesService;
import com.training.processor.dto.AbbreviationDto;
import com.training.processor.model.City;
import com.training.processor.model.CityWithAbbreviation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Component
public class CityProcessor implements ItemProcessor<City, CityWithAbbreviation> {

    private static String incorrectUrlExceptionMessage;
    private final RestTemplate restTemplate;
    private final PropertiesService propertiesService;

    @Autowired
    public CityProcessor(RestTemplate restTemplate, PropertiesService propertiesService) {
        this.restTemplate = restTemplate;
        this.propertiesService = propertiesService;
    }

    public static String getIncorrectUrlExceptionMessage() {
        return incorrectUrlExceptionMessage;
    }

    private static void setIncorrectUrlExceptionMessage(String message) {
        incorrectUrlExceptionMessage = message;
    }

    @Override
    public CityWithAbbreviation process(final City city) {
        if (city == null) {
            return null;
        }
        log.info(String.format("Start of entity processing: %s", city));
        String abbreviation = getAbbreviation(city);
        CityWithAbbreviation result = createResult(city, abbreviation);
        log.info(String.format("End of processing entity: %s", result));
        return result;
    }

    private CityWithAbbreviation createResult(City city, String abbreviation) {
        return new CityWithAbbreviation(city.getCityName(),
                city.getCounty(),
                city.getTemperature(),
                abbreviation);
    }

    private String getAbbreviation(City city) {
        String abbr = "";
        try {
            String url = createGetAbbreviationUrl(city.getCityName());
            AbbreviationDto abbrev = restTemplate.getForObject(url, AbbreviationDto.class);
            abbr = abbrev.getAbbreviation();
        } catch (Exception e) {
            setIncorrectUrlExceptionMessage(e.getMessage());
        }
        return abbr;
    }

    private String createGetAbbreviationUrl(String city) {
        return UriComponentsBuilder
                .fromHttpUrl(propertiesService.getGatewayAddress())
                .queryParam("city", city)
                .build()
                .encode()
                .toUriString();
    }
}
