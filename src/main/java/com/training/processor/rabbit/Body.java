package com.training.processor.rabbit;

import lombok.Getter;

@Getter
public class Body {
    private int count = 10;
    private String ackmode = "ack_requeue_false";
    private String encoding = "auto";
    private int truncate = 50000;

}