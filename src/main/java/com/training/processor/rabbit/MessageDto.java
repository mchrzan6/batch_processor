package com.training.processor.rabbit;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MessageDto {
    private String payload;//contains JSON string containing the whole city object.
}