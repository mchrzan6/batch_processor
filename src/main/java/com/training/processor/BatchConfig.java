package com.training.processor;

import com.training.processor.model.City;
import com.training.processor.model.CityWithAbbreviation;
import lombok.Getter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableBatchProcessing
public class BatchConfig {
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;

    @Getter
    private final ItemReader<City> reader;
    @Getter
    private final ItemProcessor<City, CityWithAbbreviation> processor;
    @Getter
    private final ItemWriter<CityWithAbbreviation> writer;

    @Autowired
    public BatchConfig(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory,
                       ItemReader<City> reader, ItemProcessor<City, CityWithAbbreviation> processor,
                       ItemWriter<CityWithAbbreviation> writer) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.reader = reader;
        this.processor = processor;
        this.writer = writer;
    }

    @Bean
    public Job importCityJob() {
        return jobBuilderFactory.get("importCityJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<City, CityWithAbbreviation>chunk(3)
                .reader(getReader())
                .processor(getProcessor())
                .writer(getWriter())
                .build();
    }
}
