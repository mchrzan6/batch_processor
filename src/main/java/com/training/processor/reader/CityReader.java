package com.training.processor.reader;

import com.training.processor.PropertiesService;
import com.training.processor.model.City;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CityReader extends FlatFileItemReader<City> {

    @Autowired
    public CityReader(PropertiesService propertiesService) {
        super();
        setupReader(propertiesService);
    }

    private void setupReader(PropertiesService propertiesService) {
        setResource(propertiesService.getCsvResource());
        setLinesToSkip(1);
        LineMapper<City> lineMapper = getCityDefaultLineMapper();
        setLineMapper(lineMapper);
        setEncoding("UTF-8");
    }

    private LineTokenizer createCityLineTokenizer() {
        ColumnAwareDelimetedLineTokenizer lineTokenizer = new ColumnAwareDelimetedLineTokenizer();
        lineTokenizer.setDelimiter(",");
        lineTokenizer.setIncludedFields(new int[]{0, 1, 2});
        String[] headers = new String[]{"cityName", "county", "temperature"};
        lineTokenizer.setNames(headers);
        return lineTokenizer;
    }

    private DefaultLineMapper<City> getCityDefaultLineMapper() {
        DefaultLineMapper<City> cityLineMapper = new DefaultLineMapper<>();
        cityLineMapper.setLineTokenizer(createCityLineTokenizer());
        cityLineMapper.setFieldSetMapper(createCityMapper());
        return cityLineMapper;
    }

    private FieldSetMapper<City> createCityMapper() {
        BeanWrapperFieldSetMapper<City> cityInformationMapper = new BeanWrapperFieldSetMapper<>();
        cityInformationMapper.setTargetType(City.class);
        return cityInformationMapper;
    }

    @Override
    protected City doRead() throws Exception {
        try {
            return super.doRead();
        } catch (FlatFileParseException e) {
            log.error(String.format("Cannot read a file. Exception : %s", e.getMessage()));
            throw e;
        }
    }

    @Override
    protected void doOpen() throws Exception {
        try {
            super.doOpen();
        } catch (IllegalStateException | ItemStreamException e) {
            log.error(String.format("Cannot initialize the reader. File does not exist?! Exception: %s",
                    e.getMessage()));
            throw e;
        }
    }
}
