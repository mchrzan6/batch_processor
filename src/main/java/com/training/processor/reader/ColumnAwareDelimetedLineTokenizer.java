package com.training.processor.reader;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.file.transform.AbstractLineTokenizer;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

@Slf4j
public class ColumnAwareDelimetedLineTokenizer extends AbstractLineTokenizer {
    public static final String DELIMITER_TAB = "\t";

    private static final String DELIMITER_COMMA = ",";

    private static final char DEFAULT_QUOTE_CHARACTER = '"';

    // the delimiter character used when reading input.
    private String delimiter;

    private char quoteCharacter = DEFAULT_QUOTE_CHARACTER;

    private String quoteString;

    private Collection<Integer> includedFields = null;

    public ColumnAwareDelimetedLineTokenizer() {
        this(DELIMITER_COMMA);
    }

    public ColumnAwareDelimetedLineTokenizer(String delimiter) {
        Assert.state(!delimiter.equals(String.valueOf(DEFAULT_QUOTE_CHARACTER)), "[" + DEFAULT_QUOTE_CHARACTER
                + "] is not allowed as delimiter for tokenizers.");

        this.delimiter = delimiter;
        setQuoteCharacter(DEFAULT_QUOTE_CHARACTER);
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    public void setIncludedFields(int[] includedFields) {
        this.includedFields = new HashSet<>();
        for (int i : includedFields) {
            this.includedFields.add(i);
        }
    }

    private void setQuoteCharacter(char quoteCharacter) {
        this.quoteCharacter = quoteCharacter;
        this.quoteString = "" + quoteCharacter;
    }

    @Override
    protected List<String> doTokenize(String line) {

        List<String> tokens = new ArrayList<>();

        // line is never null in current implementation
        // line is checked in parent: AbstractLineTokenizer.tokenize()
        char[] chars = line.toCharArray();
        boolean inQuoted = false;
        int lastCut = 0;
        int length = chars.length;
        int fieldCount = 0;
        int endIndexLastDelimiter = -1;

        for (int i = 0; i < length; i++) {
            char currentChar = chars[i];
            boolean isEnd = (i == (length - 1));

            boolean isDelimiter = isDelimiter(chars, i, delimiter, endIndexLastDelimiter);

            if (shouldParse(isEnd, isDelimiter, inQuoted)) {
                endIndexLastDelimiter = i;
                int endPosition = getEndPosition(lastCut, length, i, isEnd);

                if (isEndAndDelimiter(isEnd, isDelimiter)) {
                    endPosition = endPosition - delimiter.length();
                } else if (!isEnd) {
                    endPosition = (endPosition - delimiter.length()) + 1;
                }

                checkToken(tokens, chars, lastCut, fieldCount, endPosition);

                fieldCount++;

                if (isAndAndDelimeter(isEnd, isDelimiter)) {
                    addFieldIfConfigured(tokens, isContainFieldIfConfigured(fieldCount));
                    fieldCount++;
                }

                lastCut = i + 1;
            } else if (isQuoteCharacter(currentChar)) {
                inQuoted = !inQuoted;
            }
            log(fieldCount, hasAdditionalColumns(fieldCount));
        }

        return tokens;
    }

    private void log(int fieldCount, boolean b) {
        if (b) {
            log.warn(String.format("Line contains additional columns: %d", fieldCount));
        }
    }

    private void addFieldIfConfigured(List<String> tokens, boolean containFieldIfConfigured) {
        if (containFieldIfConfigured) {
            tokens.add("");
        }
    }

    private void checkToken(List<String> tokens, char[] chars, int lastCut, int fieldCount, int endPosition) {
        if (isContainFieldIfConfigured(fieldCount)) {
            String value = maybeStripQuotes(new String(chars, lastCut, endPosition));
            tokens.add(value);
        }
    }

    private int getEndPosition(int lastCut, int length, int i, boolean isEnd) {
        return isEnd ? (length - lastCut) : (i - lastCut);
    }

    private boolean isEndAndDelimiter(boolean isEnd, boolean isDelimiter) {
        return isEnd && isDelimiter;
    }

    private boolean shouldParse(boolean isEnd, boolean isDelimiter, boolean inQuoted) {
        return (isDelimiter && !inQuoted) || isEnd;
    }

    private boolean isContainFieldIfConfigured(int fieldCount) {
        return includedFields == null || includedFields.contains(fieldCount);
    }

    private boolean isAndAndDelimeter(boolean isEnd, boolean isDelimiter) {
        return isEnd && (isDelimiter);
    }

    private boolean hasAdditionalColumns(int fieldCount) {
        return includedFields != null && fieldCount > includedFields.size();
    }

    private String maybeStripQuotes(String string) {
        String value = string.trim();
        if (isQuoted(value)) {
            value = StringUtils.replace(value, "" + quoteCharacter + quoteCharacter, "" + quoteCharacter);
            int endLength = value.length() - 1;
            // used to deal with empty quoted values
            if (endLength == 0) {
                endLength = 1;
            }
            value = value.substring(1, endLength);
            return value;
        }
        return string;
    }


    private boolean isQuoted(String value) {
        return value.startsWith(quoteString) && value.endsWith(quoteString);
    }

    private boolean isDelimiter(char[] chars, int i, String token, int endIndexLastDelimiter) {
        boolean result = false;
        if (i - endIndexLastDelimiter >= delimiter.length() && i >= token.length() - 1) {
            String end = new String(chars, (i - token.length()) + 1, token.length());
            if (token.equals(end)) {
                result = true;
            }
        }

        return result;
    }

    private boolean isQuoteCharacter(char c) {
        return c == quoteCharacter;
    }
}
