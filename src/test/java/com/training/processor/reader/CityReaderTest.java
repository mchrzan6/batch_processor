package com.training.processor.reader;

import com.training.processor.PropertiesService;
import com.training.processor.model.City;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ParseException;
import org.springframework.core.io.ClassPathResource;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(MockitoJUnitRunner.class)
public class CityReaderTest {


    @Mock
    PropertiesService propertiesService;

    CityReader cityReader;

    private void initExecutionContextWithNotEmptyFileWithCorrectData() {
        //this file contains 3 rows of correct data
        Mockito.when(propertiesService.getCsvResource()).thenReturn(new ClassPathResource("data\\exampleDataWith3CorrectRows.csv"));
        init();
    }

    private void initExecutionContextWithIncorrectData() {
        //this file contains 2 rows of correct data and 1 incorrect, the 2nd row doesn't contain one column
        Mockito.when(propertiesService.getCsvResource()).thenReturn(new ClassPathResource("data\\exampleIncorrectData.csv"));
        init();
    }

    private void initExecutionContextWithEmptyFile() {
        //this file is empty
        Mockito.when(propertiesService.getCsvResource()).thenReturn(new ClassPathResource("data\\exampleEmptyFile.csv"));
        init();
    }

    private void initExecutionContextWitNotExistingFile() {
        //this file does not exist
        Mockito.when(propertiesService.getCsvResource()).thenReturn(new ClassPathResource("data\\exampleNotExists.csv"));
        init();
    }

    //must be called before every test
    private void init() {
        cityReader = new CityReader(propertiesService);
        ExecutionContext executionContext = new ExecutionContext();
        // cityReader.setResource(new ClassPathResource("data\\example.csv"));
        cityReader.open(executionContext);
    }

    @Test
    public void testReadingCorrectData() throws Exception {
        initExecutionContextWithNotEmptyFileWithCorrectData();
        List<City> cities = new ArrayList<>();
        City city;
        do {
            city = cityReader.read();
            cities.add(city);
        } while (city != null);
        assertEquals(3L, cities.stream().filter(Objects::nonNull).count());
    }

    @Test(expected = ParseException.class)
    public void testReadingIncorrectData() throws Exception {
        initExecutionContextWithIncorrectData();
        City city;
        do {
            city = cityReader.read();
        } while (city != null);
    }

    @Test
    public void testReadingEmptyFile() throws Exception {
        initExecutionContextWithEmptyFile();
        List<City> cities = new ArrayList<>();
        City city;
        do {
            city = cityReader.read();
            cities.add(city);
        } while (city != null);
        assertEquals(1, cities.size());
        assertNull(cities.get(0));
    }

    @Test(expected = ItemStreamException.class)
    public void testReadingNotExistingFile() throws Exception {
        initExecutionContextWitNotExistingFile();
        cityReader.read();
    }
}
