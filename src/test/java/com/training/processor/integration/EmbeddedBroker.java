package com.training.processor.integration;


import com.google.common.io.Files;
import lombok.extern.slf4j.Slf4j;
import org.apache.qpid.server.Broker;
import org.apache.qpid.server.BrokerOptions;

@Slf4j
public class EmbeddedBroker {
    public static final int BROKER_PORT = 5673;
    private final BrokerOptions options;
    private Broker broker;


    public EmbeddedBroker() {
        broker = new Broker();
        options = createBrokerOptions(BROKER_PORT);
    }

    public EmbeddedBroker(int testPort) {
        broker = new Broker();
        options = createBrokerOptions(testPort);
    }

    public void start() throws Exception {
        broker.startup(options);
    }

    private BrokerOptions createBrokerOptions(int brokerPort) {
        final String configFileName = "src/test/resources/qpid-config.json";
        final String passwordFileName = "src/test/resources/passwd.properties";
        BrokerOptions brokerOptions = new BrokerOptions();
        brokerOptions.setConfigProperty("qpid.amqp_port", String.valueOf(brokerPort));
        brokerOptions.setConfigProperty("qpid.pass_file", passwordFileName);
        brokerOptions.setConfigProperty("qpid.work_dir", Files.createTempDir().getAbsolutePath());
        brokerOptions.setInitialConfigurationLocation(configFileName);
        return brokerOptions;
    }

    public void stop() {
        try {
            broker.shutdown();
        } catch (Throwable noSuchFieldError) {
            log.error(noSuchFieldError.getMessage());
        }
    }

}
