package com.training.processor.writer;

import com.training.processor.ProcessorConfiguration;
import com.training.processor.model.CityWithAbbreviation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class QueueAdapterTest {
    @Mock
    private RabbitTemplate rabbitTemplate;

    @InjectMocks
    private QueueAdapter adapter;

    @Test
    public void shouldSendAllElementsFromList() throws Exception {
        //given
        CityWithAbbreviation firstCity = new CityWithAbbreviation("City1", "Couunty1", "12", "abr1");
        CityWithAbbreviation secondCity = new CityWithAbbreviation("City2", "Couunty2", "5", "abr2");
        CityWithAbbreviation thirdCity = new CityWithAbbreviation("City3", "Couunty3", "8", "abr3");
        CityWithAbbreviation fourthCity = new CityWithAbbreviation("City4", "Couunty4", "15", "abr4");
        List<CityWithAbbreviation> citiesToBeSent = Arrays.asList(firstCity, secondCity, thirdCity, fourthCity);
        //when
        adapter.write(citiesToBeSent);
        //then all elements from the list must be sent
        verify(rabbitTemplate, times(1)).convertAndSend(eq(ProcessorConfiguration.TEST_QUEUE_NAME), eq(firstCity));
        verify(rabbitTemplate, times(1)).convertAndSend(eq(ProcessorConfiguration.TEST_QUEUE_NAME), eq(secondCity));
        verify(rabbitTemplate, times(1)).convertAndSend(eq(ProcessorConfiguration.TEST_QUEUE_NAME), eq(thirdCity));
        verify(rabbitTemplate, times(1)).convertAndSend(eq(ProcessorConfiguration.TEST_QUEUE_NAME), eq(fourthCity));
        verifyNoMoreInteractions(rabbitTemplate);
    }

    @Test
    public void shouldNotSendAnyMessageForEmptyList() throws Exception {
        //given
        List<CityWithAbbreviation> empty = Collections.emptyList();
        //when
        adapter.write(empty);
        //then nothing to be sent
        verifyNoMoreInteractions(rabbitTemplate);
    }
}
