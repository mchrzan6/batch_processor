package com.training.processor.processor;

import com.training.processor.PropertiesService;
import com.training.processor.dto.AbbreviationDto;
import com.training.processor.model.City;
import com.training.processor.model.CityWithAbbreviation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CityProcessorTest {
    @Mock
    private RestTemplate restTemplate;

    @Mock
    private PropertiesService propertiesService;

    @InjectMocks
    private CityProcessor cityProcessor;

    private String baseUrl = "https://gateway-city-app.cfapps.io/gateway";
    private String testValue = "test1";

    @Test
    public void shouldSendRestQueryForAddress() throws Exception {
        //given
        when(propertiesService.getGatewayAddress()).thenReturn(baseUrl);
        AbbreviationDto response = new AbbreviationDto("test", testValue);
        String queryParam = "city1";
        String requestUrl = baseUrl + "?city=" + queryParam;
        City city = new City(queryParam, "count1", "12");
        when(restTemplate.getForObject(eq(requestUrl), eq(AbbreviationDto.class))).thenReturn(response);
        //when
        CityWithAbbreviation result = cityProcessor.process(city);
        //then
        verify(restTemplate, times(1)).getForObject(eq(requestUrl), eq(AbbreviationDto.class));
        assertEquals(testValue, result.getAbbreviation());
        assertEquals(city.getCityName(), result.getCityName());
        assertEquals(city.getCounty(), result.getCounty());
        assertEquals(city.getTemperature(), result.getTemperature());
        verifyNoMoreInteractions(restTemplate);
    }
}
