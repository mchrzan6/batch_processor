package com.training.connector;

import com.jcraft.jsch.*;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.awaitility.Awaitility.await;

@Slf4j
public class Connector {

    private static final Logger LOGGER = Logger.getLogger(Connector.class.getName());
    private Session session;
    private Channel channelShell;
    private ChannelSftp channelSftp;
    private InputStream streamSftp;
    private OutputStream outputStreamSftp;
    private ArrayList<String> rowsFromFile = new ArrayList<>();


    public void setConnection(String user, String host, String pass) {
        if (user.equals("jenkins")) {
            setConnectionWithPassword(user, host, pass);
        } else {
            setConnectionWithSSHkey(user, host, pass);
        }
    }

    private void setConnectionWithPassword(String user, String host, String pass) {
        try {
            JSch jsch = new JSch();
            this.session = jsch.getSession(user, host, (22));
            this.session.setPassword(pass);
            UserInfo ui = new MyUserInfo() {
                @Override
                public boolean promptYesNo(String message) {
                    return true;
                }
            };
            this.session.setUserInfo(ui);
            this.session.connect(10000);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
    }

    private void setConnectionWithSSHkey(String user, String host, String pass) {
        String privateKeyPath = String.format("C:\\Users\\%s\\.ssh\\id_rsa", System.getProperty("user.name"));
        String knownHostsPath = String.format("C:\\Users\\%s\\.ssh\\known_hosts", System.getProperty("user.name"));

        try {
            JSch jsch = new JSch();
            jsch.setKnownHosts(knownHostsPath);
            jsch.addIdentity(privateKeyPath, pass);
            this.session = jsch.getSession(user, host, (22));
            session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            UserInfo ui = new MyUserInfo() {
                @Override
                public boolean promptYesNo(String message) {
                    return true;
                }
            };
            this.session.setUserInfo(ui);
            this.session.connect(10000);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
    }

    public void setShellSession() {
        try {
            this.channelShell = this.session.openChannel("shell");
        } catch (JSchException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
    }

    public void setInputCommand(String commandForBash) {
        try {
            setShellSession();
            char enterKeyPress = (char) 13;
            String command = commandForBash + enterKeyPress;
            InputStream stream = new ByteArrayInputStream(command.getBytes(StandardCharsets.UTF_8.name()));
            this.channelShell.setInputStream(stream);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            this.channelShell.setOutputStream(outputStream);
            this.channelShell.connect(3 * 1000);
            Thread.sleep(2000);
            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.log(Level.INFO, outputStream.toString("UTF-8"));
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
    }

    public void setInputCommand(String commandForBash, String messageToWait) {
        try {
            setShellSession();
            char enterKeyPress = (char) 13;
            String command = commandForBash + enterKeyPress;
            InputStream stream = new ByteArrayInputStream(command.getBytes(StandardCharsets.UTF_8.name()));
            this.channelShell.setInputStream(stream);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            this.channelShell.setOutputStream(outputStream);
            this.channelShell.connect(3 * 1000);
            Thread.sleep(2000);
            await().atMost(2, TimeUnit.MINUTES).until(isMessageDisplayed(outputStream, messageToWait));
            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.log(Level.INFO, outputStream.toString("UTF-8"));
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
    }

    private Callable<Boolean> isMessageDisplayed(ByteArrayOutputStream output, String message) {
        return () -> {
            Boolean isEqual = false;
            String outputString = output.toString();
            List<String> outputArray = new ArrayList<>(Arrays.asList(outputString.split("\n")));
            for (String line : outputArray) {
                line = line.replaceAll("\\r","");
                if (line.equals(message)) {
                    isEqual=true;
                }
            }
            return isEqual;
        };
    }

    public void setSftpSession() {
        try {
            this.channelSftp = (ChannelSftp) this.session.openChannel("sftp");
            channelSftp.connect();
        } catch (JSchException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
    }

    public void cleanLogFileOnVirtaulMachine(String fileName, String directory) {
        try {
            String filePath = directory + fileName;
            this.outputStreamSftp = this.channelSftp.put(filePath);
        } catch (SftpException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
        OutputStreamWriter my2 = new OutputStreamWriter(this.outputStreamSftp);
        try {
            my2.write("");
            my2.close();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
    }

    public List<String> readFromFile(String fileName, String directory) {
        try {
            String filePath = directory + fileName;
            this.streamSftp = this.channelSftp.get(filePath);
        } catch (SftpException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
        InputStreamReader myReader = new InputStreamReader(this.streamSftp);
        try {
            BufferedReader buffReader = new BufferedReader(myReader);
            String row;
            while ((row = buffReader.readLine()) != null) {
                rowsFromFile.add(row);
            }
            myReader.close();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
        return rowsFromFile;
    }

    public void disconnectSession() {
        this.session.disconnect();
    }

    abstract class MyUserInfo
            implements UserInfo, UIKeyboardInteractive {
        public String getPassword() {
            return null;
        }

        public boolean promptYesNo(String str) {
            return false;
        }

        public String getPassphrase() {
            return null;
        }

        public boolean promptPassphrase(String message) {
            return false;
        }

        public boolean promptPassword(String message) {
            return false;
        }

        public void showMessage(String message) {
        }

        public String[] promptKeyboardInteractive(String destination,
                                                  String name,
                                                  String instruction,
                                                  String[] prompt,
                                                  boolean[] echo) {
            return new String[0];
        }
    }
}
