package cucumber.stepdefs;

import com.training.processor.ProcessorApplication;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HooksDefs {
    private static boolean isBeforeAllStartedSprint2 = false;

    @Before("@Sprint2")
    public void beforeScenario(Scenario scenario) {
        log.info("\n Name: "+scenario.getName()+", status: " + scenario.getStatus());
        System.out.println("This will run before the Scenario");
    }


    @After("@Sprint2")
    public void afterScenario() {
        ProcessorApplication.closeApp();
    }


    //BeforeAll
    @Before("@Sprint2")
    public static void beforeAll() {
        if (!isBeforeAllStartedSprint2) {
            log.info("BeforeAll step was ran");
            isBeforeAllStartedSprint2 = true;
        }
    }
}

