package cucumber.stepdefs;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.training.connector.Connector;
import com.training.processor.ProcessorApplication;
import com.training.processor.ProcessorConfiguration;
import com.training.processor.PropertiesService;
import com.training.processor.integration.EmbeddedBroker;
import com.training.processor.model.City;
import com.training.processor.model.CityWithAbbreviation;
import com.training.processor.processor.CityProcessor;
import com.training.processor.rabbit.Body;
import com.training.processor.rabbit.MessageDto;
import com.training.processor.reader.CityReader;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.junit.Assert;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.apache.commons.lang.exception.ExceptionUtils.getMessage;
import static org.awaitility.Awaitility.await;
import static org.junit.Assert.*;

@Slf4j
public class ScenariosStepDefs {

    private static final String PROCESSOR_PASSW = "processor";
    private static final String PATH_DATA = "src/main/resources/data/";
    private static final String PATH_LOG = "";
    private static final String ERROR_MESSAGE = "Something went wrong with BufferedReader";
    private static final String WARNING_MESSAGE = "Line contains additional columns";
    private static final String PROCESSOR_FILE = "processor.log";
    private ArrayList<String> rowsFromReader = new ArrayList<>();
    private ArrayList<String> rowsFromFile = new ArrayList<>();
    private List<String> rowsFromKafka = new ArrayList<>();
    private List<String> rowsFromHbase = new ArrayList<>();
    private String exceptionMessage;
    private Connector connector;
    private String fullPcfUrl;
    private String encryptedDevIP = "8DJ3BlMmKUaLmM/GpxbtPV19lyD6mApA";
    private String encryptedTestIP = "o4ukuMOfQhJDfNHokcK7samBrjOIPBth";
    private String encryptedProdIP = "78fQxJpIiEUWVwAUhrl7Ag=="; // empty values
    private List<String> encryptedKnownIPServers = Arrays.asList(encryptedDevIP, encryptedTestIP, encryptedProdIP);
    private ArrayList<String> decryptedKnownIPServers = new ArrayList<>();
    private static final String HTTP = "http://";
    private String ecryptedPassJenkins = "QwoHXRQAYHZHnHeKmOU0fDO81ZXKHNd5";
    private String encryptedPassRoot = "G+xmj4S4ql4yu5T+T517ZxtWTamREsGz";
    private String getResponse;
    private String putResponse;

    private String createFilePath(String fileName, String path) {
        return path + fileName.replace(" ", "");
    }

    private String getFileExtension(File file) {
        String fileName = file.getName();
        if (fileName.lastIndexOf('.') != -1 && fileName.lastIndexOf('.') != 0)
            return fileName.substring(fileName.lastIndexOf('.') + 1);
        else return "";
    }

    private boolean ifFileExists(String fileName) {
        File[] files = new File(PATH_DATA).listFiles();
        boolean isFileExists = false;
        isFileExists = Arrays.stream(files)
                .map(File::getName)
                .anyMatch(name -> name.contains(fileName.replace(" ", "")));
        return isFileExists;
    }

    @Given("^File doesn't exist(.+)$")
    public void checkIfFileDoesntExists(String fileName) {
        assertFalse("File shouldn't exist, but it exists", ifFileExists(fileName));
    }

    @Given("^File exist(.+)$")
    public void checkIfFileExist(String fileName) {
        rowsFromReader.clear();
        assertTrue("File doesn't exist", ifFileExists(fileName));
    }

    @Then("^I start reader with (.*) and I should catch correct exception (.*)$")
    public void shouldThrowExceptionWhenFileDoesntExist(String fileName, String message) {
        try {
            CityReader cityRead = new CityReader(new PropertiesService("data\\" + fileName, ""));
            cityRead.open(new ExecutionContext());
            Assert.fail("Exception is not thrown");
        } catch (Exception exception) {
            assertEquals(message, exception.getMessage());
        }
    }

    private int countNumberOfLinesInFile(String filePath) throws IOException {
        try (BufferedReader buffReader = new BufferedReader(new FileReader(filePath));) {
            int rowCounter = 0;
            String line = null;
            while ((line = buffReader.readLine()) != null) {
                rowCounter++;
            }
            return rowCounter;
        }
    }

    @And("^I start Reader(.+)$")
    public void checkIfReaderHasStarted(String fileName) throws Exception {
        String filePath = createFilePath(fileName, PATH_DATA);
        CityReader cityRead = new CityReader(new PropertiesService("data\\" + fileName.replace(" ", ""), ""));
        cityRead.open(new ExecutionContext());
        int rowCounter = countNumberOfLinesInFile(filePath);

        if (rowCounter > 0) {
            try {
                for (int i = 0; i < rowCounter - 1; i++) {
                    City c = cityRead.read();
                    rowsFromReader.add(c.getCityName() + "," + c.getCounty() + "," + c.getTemperature());
                }
            } catch (FlatFileParseException e) {
                exceptionMessage = e.getMessage();
                log.info("Exception caught: " + exceptionMessage);
            }
        }
    }

    @Then("^I should see correct error message (.*)$")
    public void validateErrorMessage(String message) {
        assertTrue("Incorrect exception message", exceptionMessage.contains(message));
    }

    @And("^File has format csv(.+)$")
    public void checkIfFileFormatIsCsv(String fileName) {
        File file = new File(createFilePath(fileName, PATH_DATA));
        String format = getFileExtension(file);
        assertTrue("This is not csv file", format.equals("csv"));
    }

    @And("^File has format different than csv(.+)$")
    public void checkIfFileHasWrongFormat(String fileName) {
        File file = new File(PATH_DATA + fileName);
        String format = getFileExtension(file);
        assertFalse("This is csv file, but shouldn't", format.equals("csv"));
    }

    @And("^File is empty(.+)$")
    public void checkIfFileIsEmpty(String fileName) {
        String filePath = createFilePath(fileName, PATH_DATA);
        try {
            try (BufferedReader buffReader = new BufferedReader(new FileReader(filePath))) {
                Assert.assertNull("File is not empty", buffReader.readLine());
            }
        } catch (IOException e) {
            log.info(ERROR_MESSAGE);
        }
    }

    @And("^File is not empty(.+)$")
    public void checkIfFileIsNotEmpty(String fileName) {
        String filePath = createFilePath(fileName, PATH_DATA);
        try {
            try (BufferedReader buffReader = new BufferedReader(new FileReader(filePath))) {
                Assert.assertNotNull("File is empty", buffReader.readLine());
            }
        } catch (IOException e) {
            log.info(ERROR_MESSAGE);
        }
    }

    @And("^File has a wrong separator(.+)$")
    public void checkIfSeparatorIsWrong(String fileName) {
        String filePath = createFilePath(fileName, PATH_DATA);
        int counterComa = 0;
        int counterSemicolon = 0;
        try {
            try (BufferedReader buffReader = new BufferedReader(new FileReader(filePath))) {
                for (int i = 0; i < 4; i++) {
                    String temporaryLine = buffReader.readLine();
                    counterComa = counterComa + StringUtils.countOccurrencesOf(temporaryLine, ",");
                    counterSemicolon = counterSemicolon + StringUtils.countOccurrencesOf(temporaryLine, ";");
                }
                assertFalse("In this method separator should be incorrect", counterComa > counterSemicolon);
            }
        } catch (IOException e) {
            log.info(ERROR_MESSAGE);
        }
    }

    @And("^File has a correct separator(.+)$")
    public void checkIfSeparatorIsCorrect(String fileName) {
        String filePath = createFilePath(fileName, PATH_DATA);
        int counterComa = 0;
        int counterSemicolon = 0;
        try {
            try (BufferedReader buffReader = new BufferedReader(new FileReader(filePath))) {
                for (int i = 0; i < 4; i++) {
                    String temporaryLine = buffReader.readLine();
                    counterComa = counterComa + StringUtils.countOccurrencesOf(temporaryLine, ",");
                    counterSemicolon = counterSemicolon + StringUtils.countOccurrencesOf(temporaryLine, ";");
                }
                assertTrue("In this method separator should be incorrect", counterComa > counterSemicolon);
            }
        } catch (IOException e) {
            log.info(ERROR_MESSAGE);
        }
    }

    @Then("^File has a correct data(.+)$")
    public void checkIfFileHasACorrectData(String fileName) throws IOException {
        String filePath = createFilePath(fileName, PATH_DATA);
        try (BufferedReader buffReader = new BufferedReader(new FileReader(filePath))) {
            String line = null;
            int lineCounter = 0;

            while ((line = buffReader.readLine()) != null) {
                if (lineCounter != 0) {
                    rowsFromFile.add(line);
                }
                lineCounter++;
            }
        }
        assertTrue("Data from reader is different than in CSV", rowsFromReader.toString().equals(rowsFromFile.toString()));
    }

    @Then("^ArrayList should be empty$")
    public void arraylistShouldBeEmpty() {
        assertTrue("ArrayList is not empty", rowsFromFile != null);
    }

    private String getArgumentToRun(String environment, String processorRunningArgument) throws UnknownHostException {
        String environmentIP = getIPaddressOfEnvironment(environment);
        String serverIP = getIPaddressWhereProcessorIsRunning();
        return decryptedKnownIPServers.contains(serverIP) ? processorRunningArgument + serverIP : processorRunningArgument + environmentIP;
    }

    @Given("^I start processor with environment: (.+) and Processor running argument (.+)$")
    public void startProcessor(String environment, String processorRunningArgument) throws UnknownHostException {
        try {
            cleanLogFile(createFilePath(PROCESSOR_FILE, PATH_LOG));
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        }
        ProcessorApplication.main(new String[]{getArgumentToRun(environment, processorRunningArgument)});
        await().atMost(2, TimeUnit.MINUTES).until(isMessageDisplayed());
    }

    @Given("^I start processor with dedicated input csv file (.+), environment: (.+) and Processor running argument (.+)$")
    public void startProcessorWithDedicatedCsvFile(String fileName, String environment, String processorRunningArgument) throws UnknownHostException {
        try {
            cleanLogFile(createFilePath(PROCESSOR_FILE, PATH_LOG));
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        }
        String filePath = "--app.csv.path=data\\" + fileName;
        filePath = filePath.replaceAll("\\s", "");
        ProcessorApplication.main(new String[]{filePath, getArgumentToRun(environment, processorRunningArgument)});
        await().atMost(2, TimeUnit.MINUTES).until(isMessageDisplayed());
    }

    public void cleanLogFile(String filePath) throws FileNotFoundException {
        try (PrintWriter writer = new PrintWriter(filePath)) {
            writer.print("");
        }
    }

    private Callable<Boolean> isMessageDisplayed() {
        return () -> {
            Boolean isEqual = false;
            readCSVFileAndSaveDataToArray(PROCESSOR_FILE, PATH_LOG);
            for (String line : rowsFromFile) {
                line = line.replaceAll("\\r", "");
                if (line.contains("Started ProcessorApplication in")) {
                    isEqual = true;
                }
            }
            return isEqual;
        };
    }

    @Given("^I start processor with incorrect url (.+), environment: (.+) and Processor running argument (.+)$")
    public void startProcessorWithIncorrectPcfUrl(String incorrectPcfUrl, String environment, String processorRunningArgument) throws UnknownHostException {
        this.fullPcfUrl = incorrectPcfUrl;
        ProcessorApplication.main(new String[]{"--app.gateway.address=" + incorrectPcfUrl, getArgumentToRun(environment, processorRunningArgument)});
    }

    @When("^I start processor with embedded rabbit and with environment: (.+) and Processor running argument (.+)$")
    public void startProcessorWithEmbeddedRabbit(String environment, String processorRunningArgument) throws UnknownHostException {
        ProcessorApplication.main(new String[]{"--spring.rabbitmq.port=5673", getArgumentToRun(environment, processorRunningArgument)});
        await().atMost(2, TimeUnit.MINUTES).until(isMessageDisplayed());
    }

    @Then("^I should get correct exception message on processor output(.+)$")
    public void msg(String messageFromProcessor) {
        String exceptionMsg = "[" + this.fullPcfUrl + "]" + messageFromProcessor;
        assertTrue("Exception message on Processor output is incorrect", exceptionMsg.equals(CityProcessor.getIncorrectUrlExceptionMessage()));
    }

    @Given("^I close processor$")
    public void closeProcessor() {
        ProcessorApplication.closeApp();
    }

    @Given("^I run connector .jar file$")
    public void runConnectorJarFile() {
        connector.setShellSession();
        connector.setInputCommand("java -jar connector_app.jar");
    }

    @Given("^I read CSV file(.+)$")
    public void readCSVFileAndSaveDataToArray(String fileName) throws IOException {
        rowsFromFile.clear();
        String filePath = createFilePath(fileName, PATH_DATA);
        try (BufferedReader buffReader = new BufferedReader(new FileReader(filePath))) {
            String line = null;
            int lineCounter = 0;

            while ((line = buffReader.readLine()) != null) {
                if (lineCounter != 0) {
                    rowsFromFile.add(line);
                }
                lineCounter++;
            }
        }
    }

    private void readCSVFileAndSaveDataToArray(String fileName, String path) throws IOException {
        rowsFromFile.clear();
        String filePath = createFilePath(fileName, path);
        try (BufferedReader buffReader = new BufferedReader(new FileReader(filePath))) {
            String line = null;
            int lineCounter = 0;

            while ((line = buffReader.readLine()) != null) {
                if (lineCounter != 0) {
                    rowsFromFile.add(line);
                }
                lineCounter++;
            }
        }
    }

    @When("^I read (.*) file from directory (.*) on Virtual Machine$")
    public void readKafkaFileAndSaveDataToArray(String fileName, String directory) {
        rowsFromKafka.clear();

        rowsFromKafka = connector.readFromFile(fileName, directory);
    }

    @When("^I read Hbase file (.*) from directory (.*) on server$")
    public void readHbaseFileAndSaveDataToArray(String fileName, String directory) {
        rowsFromHbase.clear();
        rowsFromHbase = connector.readFromFile(fileName, directory);
        log.info(rowsFromHbase.toString());
    }

    @Then("^Data in input file matches output file$")
    public void checkIfDataInKafkaIsCorrect() {
        int coutnerKafkaLine = 0;
        if (rowsFromKafka.isEmpty()) {
            fail("Message didn't reach Kafka, some processes are not running");
        } else {
            for (String line : rowsFromKafka) {
                try {
                    String[] elements = rowsFromFile.get(coutnerKafkaLine).split(",");
                    for (String element : elements) {
                        assertTrue(element + " doesn't exist in right row in Kafka message", line.contains(element));
                    }
                } catch (IndexOutOfBoundsException e) {
                    log.error(e.getMessage());
                    fail("Kafka had more messages than the Processor sent");
                }
                coutnerKafkaLine++;
            }
        }
    }

    @Given("^I connect to Virtual Machine$")
    public void connectToVirtualMachine() {
        try {
            connector = new Connector();
            connector.setConnection("discover", "127.0.0.1", "admin");
            connector.setSftpSession();
            connector.setShellSession();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private void decryptListOfServers() {
        for (int i = 0; i < encryptedKnownIPServers.size(); i++) {
            decryptedKnownIPServers.add(decryptPassword(encryptedKnownIPServers.get(i)));
        }
    }

    @Given("^I connect to Server: (.+) with User: (.+)$")
    public void connectToVirtualMachine(String environment, String user) throws UnknownHostException {
        String environmentIP = getIPaddressOfEnvironment(environment);

        String password = user.equals("jenkins") ? decryptPassword(ecryptedPassJenkins) : decryptPassword(encryptedPassRoot);

        String serverIP = getIPaddressWhereProcessorIsRunning();
        decryptListOfServers();

        String ipToRun = null;

        ipToRun = decryptedKnownIPServers.contains(serverIP) ? serverIP : environmentIP;

        try {
            connector = new Connector();
            connector.setConnection(user, ipToRun, password);
            connector.setSftpSession();
            connector.setShellSession();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public String getIPaddressOfEnvironment(String environment) {
        switch (environment) {
            case "DEV":
                return decryptPassword(encryptedDevIP);
            case "TEST":
                return decryptPassword(encryptedTestIP);
            case "PROD":
                return decryptPassword(encryptedProdIP);
            default:
                log.error("Wrong argument for environment to run");
                return null;
        }
    }

    @And("^I run (.+) command in console and wait for (.+)$")
    public void runCommandInVirtualMachine(String command, String messageToWait) {
        try {
            connector.setInputCommand(command, messageToWait);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @And("^I run command in console (.+)$")
    public void runCommandInVirtualMachine(String command) {
        try {
            connector.setInputCommand(command);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private File connectWithProcessorLogFile(String fileName) {
        File folder = new File("src/");
        String processorLogFolder = folder.getAbsoluteFile().getParent() + "/";
        return new File(processorLogFolder + fileName);
    }

    @And("^I clean processor log file (.+)$")
    public void cleanProcessorLogFile(String fileName) {
        try {
            FileWriter fileWriter = new FileWriter(connectWithProcessorLogFile(fileName));
            fileWriter.close();
        } catch (Exception e) {
            ScenariosStepDefs.log.error(e.getMessage());
        }
    }

    @Given("^I clean file (.*) in indicated directory (.*) on server")
    public void cleanFileInDirectoryOnVirtualMachine(String fileName, String directory) {
        connector.cleanLogFileOnVirtaulMachine(fileName, directory);
    }

    @Given("^I wait (.*) miliseconds$")
    public void waitSeconds(long milisecondsToWait) {
        try {
            Thread.sleep(milisecondsToWait);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    @When("^I read output file (.+) from HBase$")
    public void readFileFromHbase(String fileName) throws IOException {
        String filePath = createFilePath(fileName, PATH_DATA);
        try (BufferedReader buffReader = new BufferedReader(new FileReader(filePath))) {
            String line = null;
            while ((line = buffReader.readLine()) != null) {
                rowsFromHbase.add(line);
            }
        }
    }

    @Then("^Data in CSV file matches data saved in Hbase$")
    public void checkIfCSVFileMatchesHbaseData() {
        int i = 6;
        Collections.sort(rowsFromFile);
        try {
            for (String lineCSV : rowsFromFile) {
                String[] elements = lineCSV.split(",");
                log.info("Row number" + i + " from Hbase is: " + rowsFromHbase.get(i));
                log.info("City: " + elements[0]);
                log.info("County: " + elements[1]);
                log.info("Temperature: " + elements[2]);
                String message = "Hbase didn't return city ";
                assertTrue(message + elements[0], rowsFromHbase.get(i).contains(elements[0]));
                assertTrue(message + elements[0], rowsFromHbase.get(i + 1).contains(elements[0]));
                assertTrue(message + elements[0], rowsFromHbase.get(i + 2).contains(elements[0]));
                assertTrue("Incorrect county " + elements[1] + " for city " + elements[0], rowsFromHbase.get(i + 1).contains(elements[1]));
                assertTrue("Incorrect temperature " + elements[2] + " for city " + elements[0], rowsFromHbase.get(i + 2).contains(elements[2]));
                i += 3;

            }
        } catch (IndexOutOfBoundsException e) {
            log.error(e.getMessage());
            Assert.fail("Log file form HBase is empty, something went wrong");
        }
    }

    @Then("^I disconnect to Virtual Machine$")
    public void disconnectToVirtualMachine() {
        connector.disconnectSession();
    }

    @Given("^I wait for all processes be alive$")
    public void iWaitForAllProcessesBeAlive() {
        try {
            Thread.sleep(400000);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    @Then("^I should get correct mapping in Rabbit MQ on server (.+)$")
    public void validateMappingOnRabbitOutput(String environment) {
        RestTemplate restTemplate = createRestTemplate();
        String environmentIP = getIPaddressOfEnvironment(environment);
        String url = HTTP + environmentIP + ":15672/api/queues/vhost/test-queue/get";
        String processorUsernameAndPassword = this.getEncryptedProcessorPassword();
        MultiValueMap<String, String> headers = getHeaders(processorUsernameAndPassword, processorUsernameAndPassword);
        HttpEntity tHttpEntity = new HttpEntity<Body>(new Body(), headers);
        ResponseEntity<MessageDto[]> response = restTemplate.exchange(url, HttpMethod.POST,
                tHttpEntity, MessageDto[].class);
        Gson gson = new Gson();
        List<CityWithAbbreviation> cities = Arrays.stream(response.getBody())
                .map(MessageDto::getPayload)
                .map(o -> mapToCity(o, gson))
                .collect(Collectors.toList());
        cities.forEach(c -> assertTrue("Abbrevation is incorrect", findString(c.toString())));
    }

    @And("^Processor log file (.+) contains correct warning$")
    public void validateWarningMessageInProcessorLogFile(String fileName) {
        try (Scanner scanner = new Scanner(connectWithProcessorLogFile(fileName))) {
            boolean containsWarning = false;
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.contains(WARNING_MESSAGE)) {
                    containsWarning = true;
                }
            }
            assertTrue("Processor log file doesn't contain warning message", containsWarning);
        } catch (FileNotFoundException e) {
            ScenariosStepDefs.log.error(e.getMessage());
        }
    }

    private String getEncryptedProcessorPassword() {
        return PROCESSOR_PASSW;
    }

    private boolean findString(String inputString) {
        boolean abbrevationMatch = true;
        if (inputString.contains("Wroclaw") && (!inputString.contains("Wro"))) {
            abbrevationMatch = false;
        }
        if (inputString.contains("Warszawa") && (!inputString.contains("Wwa"))) {
            abbrevationMatch = false;
        }
        if (inputString.contains("Berlin") && (!inputString.contains("Ber"))) {
            abbrevationMatch = false;
        }
        if (inputString.contains("Wr") && (!inputString.contains("Wr"))) {
            abbrevationMatch = false;
        }
        if (inputString.contains("Wa") && (!inputString.contains("Wa"))) {
            abbrevationMatch = false;
        }
        if (inputString.contains("Be") && (!inputString.contains("Be"))) {
            abbrevationMatch = false;
        }
        return abbrevationMatch;
    }

    private CityWithAbbreviation mapToCity(String o, Gson gson) {
        return gson.fromJson(o, CityWithAbbreviation.class);
    }

    private RestTemplate createRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        GsonHttpMessageConverter converter = new GsonHttpMessageConverter();
        restTemplate.setMessageConverters(Collections.singletonList(converter));
        return restTemplate;
    }

    private MultiValueMap<String, String> getHeaders(String username, String password) {
        HttpHeaders httpHeaders = new HttpHeaders();
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes());
        String authHeader = "Basic " + new String(encodedAuth);
        httpHeaders.add("Authorization", authHeader);
        httpHeaders.add("Content-Type", "application/json");
        return httpHeaders;
    }

    public String getIPaddressWhereProcessorIsRunning() throws UnknownHostException {
        InetAddress localhost = InetAddress.getLocalHost();
        return localhost.getHostAddress();
    }

    @And("^Get data (.+) (.+)$")
    public void getData(String url, String environment) throws IOException {
        String environmentIP = getIPaddressOfEnvironment(environment);
        CloseableHttpClient client = null;
        try {
            client = HttpClients.createDefault();
            String serverIP = getIPaddressWhereProcessorIsRunning();
            String ipToRun = null;
            ipToRun = decryptedKnownIPServers.contains(serverIP) ? serverIP : environmentIP;
            HttpGet request = new HttpGet(HTTP + ipToRun + url);
            HttpResponse response = client.execute(request);
            org.apache.http.HttpEntity entity = response.getEntity();
            if (entity != null) {
                getResponse = convertResponseToString(response);
                log.info(String.format("This is a get response: %s%n", getResponse));
            }
        } catch (IOException exception) {
            log.error(exception.getMessage());
        } finally {
            if (client != null) {
                client.close();
            }
        }
    }

    private String convertResponseToString(HttpResponse response) throws IOException {
        InputStream responseStream = response.getEntity().getContent();
        Scanner scanner = new Scanner(responseStream, "UTF-8");
        String receivedResponse = scanner.useDelimiter("\\Z").next();
        scanner.close();
        return receivedResponse;
    }

    @Given("^I put (.+) data with url (.+) (.+)$")
    public void postData(String fileName, String url, String environment) throws IOException {
        String environmentIP = getIPaddressOfEnvironment(environment);
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClients.createDefault();
            String filePath = createFilePath(fileName, PATH_DATA);
            String jsonData = scanFile(filePath);
            String serverIP = getIPaddressWhereProcessorIsRunning();
            String ipToRun = null;
            ipToRun = decryptedKnownIPServers.contains(serverIP) ? serverIP : environmentIP;
            HttpPut request = new HttpPut(HTTP + ipToRun + url);
            StringEntity entity = new StringEntity(jsonData);
            request.addHeader("content-type", "application/json");
            request.setEntity(entity);
            HttpResponse response = httpClient.execute(request);
            putResponse = convertResponseToString(response);
            log.info("This is a put response: " + putResponse);
        } catch (java.lang.Exception exception) {
            log.error(exception.getMessage());
        } finally {
            if (httpClient != null) {
                httpClient.close();
            }
        }
    }

    private String scanFile(String filePath) {
        String jsonData = null;
        try (Scanner scanner = new Scanner(new File(filePath)).useDelimiter("\\Z")) {
            jsonData = scanner.next();
        } catch (java.lang.Exception exception) {
            log.error(exception.getMessage());
        }
        return jsonData;
    }

    @Then("^Received data matches posted data (.+)$")
    public void receivedDataMatchesPostedData(String fileName) throws IOException {
        String filePath = createFilePath(fileName, PATH_DATA);
        String jsonData = null;
        try (Scanner scanner = new Scanner(new File(filePath)).useDelimiter("\\Z")) {
            jsonData = scanner.next();
        } catch (java.lang.Exception exception) {
            log.error(exception.getMessage());
        }
        CityWithAbbreviation[] fromJson = parseInput(jsonData);
        CityWithAbbreviation[] fromResponse = parseInput(getResponse);
        log.info("this is json data: " + Arrays.toString(fromJson) + "\n");
        log.info("this is response data: " + Arrays.toString(fromResponse) + "\n");
        assertArrayEquals(fromJson, fromResponse);
    }

    private CityWithAbbreviation[] parseInput(String text) {
        JsonArray jsonElements = new Gson().fromJson(text, JsonArray.class);
        return new Gson().fromJson(jsonElements, CityWithAbbreviation[].class);
    }

    @Then("^Received city row matches posted row (.+)$")
    public void receivedRowMatchesPostedRow(String fileName) {
        String filePath = createFilePath(fileName, PATH_DATA);
        String jsonData = null;
        try (Scanner scanner = new Scanner(new File(filePath)).useDelimiter("\\Z");
        ) {
            jsonData = scanner.next();
        } catch (FileNotFoundException e) {
            log.error(getMessage(e));
        }
        log.info("this is jason data: " + jsonData + "\n");
        log.info("this is response data: " + getResponse + "\n");
        Gson gson = new Gson();
        CityWithAbbreviation fromJson = gson.fromJson(jsonData, CityWithAbbreviation.class);
        CityWithAbbreviation fromResponse = gson.fromJson(getResponse, CityWithAbbreviation.class);
        assertEquals("", fromJson, fromResponse);
    }

    @Then("^I received response error (.+)$")
    public void checkIfErrorMessageReceived(String error) {
        assertTrue("Http Put response error incorrect", putResponse.contains(error));
        assertTrue("Http Get response isn't empty", getResponse.equals("[]"));
    }

    @And("^I run embedded Rabbit MQ$")
    public void startEmbeddedRabbbit() {
        try {
            EmbeddedBroker embBroker = new EmbeddedBroker();
            embBroker.start();
            createExchange(ProcessorConfiguration.TEST_QUEUE_NAME);
        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
    }

    @And("^I close embedded Rabbit MQ$")
    public void closeEmbeddedRabbit() {
        try {
            EmbeddedBroker embBroker = new EmbeddedBroker();
            embBroker.stop();
        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
    }

    @And("^I run another connector$")
    public void startAnotherConnector() {
        connector.setShellSession();
        connector.setInputCommand("java -jar /home/discover/connector_app.jar --spring.rabbitmq.port=5673 --logging.file=testprocessor.log");
    }

    private void createExchange(final String identifier) {
        final CachingConnectionFactory cf = new CachingConnectionFactory(EmbeddedBroker.BROKER_PORT);
        cf.setUsername(PROCESSOR_PASSW);
        cf.setPassword(PROCESSOR_PASSW);
        final RabbitAdmin admin = new RabbitAdmin(cf);
        final org.springframework.amqp.core.Queue queue = new Queue(identifier, true);
        admin.declareQueue(queue);
        final TopicExchange exchange = new TopicExchange("default");
        admin.declareExchange(exchange);
        admin.declareBinding(BindingBuilder.bind(queue).to(exchange).with(identifier));
        cf.destroy();
    }

    public String encryptPassword() {
        String plainPass = "";
        String jasyptPassword = System.getenv("jasypt_password");
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(jasyptPassword);
        return encryptor.encrypt(plainPass);
    }

    private String decryptPassword(String encryptedPassword) {
        String jasyptPassword = System.getenv("jasypt_password");
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(jasyptPassword);
        return encryptor.decrypt(encryptedPassword);
    }
}