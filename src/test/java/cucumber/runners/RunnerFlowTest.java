package cucumber.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;

@Slf4j
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/cucumber/sprint1.feature", "src/test/resources/cucumber/sprint2.feature",
                "src/test/resources/cucumber/sprint3.feature"},
        glue = {"cucumber.stepdefs", "cucumber.hooks"},
        plugin = {"pretty", "json:target/cucumber-reports/scenarios.json"}
)
public class RunnerFlowTest {

}
