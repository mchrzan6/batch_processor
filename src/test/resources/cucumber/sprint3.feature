Feature: sprint3

  @Sprint2
  Scenario Outline: Processor should return correct exception message when PCF url is incorrect
    Given I connect to Server: <environment> with User: <user>
    When I start processor with incorrect url <PCFurl>, environment: <environment> and Processor running argument <processorRunningArgument>
    Then I should get correct exception message on processor output <message>

    Examples:
      | environment | user    | processorRunningArgument | PCFurl       | message                 |
      | DEV         | jenkins | --spring.rabbitmq.host=  |incorrectURL| is not a valid HTTP URL |

  @Sprint2
  Scenario Outline: Check data saved in HBase and verify warning message in processor log file when csv file has additional columns

    Given I connect to Server: <environment> with User: <user>
    And I clean file <logFile> in indicated directory <directory> on server
    And I clean processor log file <processorLogFile>
    And I run <command> command in console and wait for <message>
    And I start processor with dedicated input csv file <inputFile>, environment: <environment> and Processor running argument <processorRunningArgument>
    And I read CSV file <fileName>
    And I run <command2> command in console and wait for <message2>
    When I read Hbase file <logFile> from directory <directory> on server
    Then Data in CSV file matches data saved in Hbase
    And Processor log file <processorLogFile> contains correct warning

    Examples:
      | environment | user    | processorRunningArgument | logFile     | directory            | processorLogFile | command                         | inputFile     | fileName    | message               | command2                                                                                                                | message2            |
      | DEV         | jenkins |--spring.rabbitmq.host=   | myFileHBase | /home/discover/logs/ | processor.log    | /home/discover/refresh_table.sh | Scenario8.csv | example.csv | Hbase::Table - cities | (echo "scan 'cities'" \| /usr/local/hbase/bin/hbase shell > /home/discover/logs/myFileHBase); echo "Saving in file done"| Saving in file done |


  Scenario Outline: Post and get city table from Hbase

    Given I connect to Server: <environment> with User: <user>
    And I run <command> command in console and wait for <message>
    When I put <fileName> data with url <urlPut> <environment>
    And Get data <urlGet> <environment>
    Then Received data matches posted data <fileName>

    Examples:
      | fileName        | command                         | environment | user    | urlPut       | urlGet       | message               |
      | citiesData.json | /home/discover/refresh_table.sh | DEV        | jenkins | :8087/cities | :8087/cities | Hbase::Table - cities |

  Scenario Outline: Post and get city row from Hbase

    Given I connect to Server: <environment> with User: <user>
    And I run <command> command in console and wait for <message>
    When I put <fileName> data with url <urlPut> <environment>
    And Get data <urlGet> <environment>
    Then Received city row matches posted row <fileName>

    Examples:
      | fileName          | command                         | environment | user    | urlPut               | urlGet                                 | message               |
      | warszawaData.json | /home/discover/refresh_table.sh | DEV        | jenkins | :8087/cities/cityRow | :8087/cities/cityRow?cityName=Warszawa | Hbase::Table - cities |

  Scenario Outline: Post and get temp from Hbase

    Given I connect to Server: <environment> with User: <user>
    And I run <command> command in console and wait for <message>
    When I put <fileName> data with url <urlPut> <environment>
    And Get data <urlGet> <environment>
    Then Received city row matches posted row <fileName>

    Examples:
      | fileName             | command                         | environment | user    | urlPut                   | urlGet                                    | message               |
      | wroclawTempData.json | /home/discover/refresh_table.sh | DEV        | jenkins | :8087/cities/temperature | :8087/cities/temperature?cityName=Wroclaw | Hbase::Table - cities |

  Scenario Outline: Post and get abbreviation from Hbase

    Given I connect to Server: <environment> with User: <user>
    And I run <command> command in console and wait for <message>
    When I put <fileName> data with url <urlPut> <environment>
    And Get data <urlGet> <environment>
    Then Received city row matches posted row <fileName>

    Examples:
      | fileName           | command                         | environment | user    | urlPut                    | urlGet                                    | message               |
      | berlinAbbData.json | /home/discover/refresh_table.sh | DEV        | jenkins | :8087/cities/abbreviation | :8087/cities/abbreviation?cityName=Berlin | Hbase::Table - cities |

  Scenario Outline: Post and get county from Hbase

    Given I connect to Server: <environment> with User: <user>
    And I run <command> command in console and wait for <message>
    When I put <fileName> data with url <urlPut> <environment>
    And Get data <urlGet> <environment>
    Then Received city row matches posted row <fileName>

    Examples:
      | fileName                | command                         | environment  | user    | urlPut              | urlGet                                | message               |
      | katowiceCountyData.json | /home/discover/refresh_table.sh | DEV         | jenkins | :8087/cities/county | :8087/cities/county?cityName=Katowice | Hbase::Table - cities |

  Scenario Outline: Post data without mandatory columns

    Given I connect to Server: <environment> with User: <user>
    And I run <command> command in console and wait for <message>
    When I put <fileName> data with url <urlPut> <environment>
    And Get data <urlGet> <environment>
    Then I received response error <error>

    Examples:
      | fileName             | command                         | environment | user    | urlPut                     | urlGet        | error                              | message               |
      | incorrectCities.json | /home/discover/refresh_table.sh | DEV        | jenkins | :8087/cities               | :8087/cities  | "status":400,"error":"Bad Request" | Hbase::Table - cities |
      | incorrectRow.json    | /home/discover/refresh_table.sh | DEV        | jenkins | :8087/cities/cityRow       | :8087/cities  | "status":400,"error":"Bad Request" | Hbase::Table - cities |
      | missingTemp.json     | /home/discover/refresh_table.sh | DEV        | jenkins | :8087/cities/temperature   | :8087/cities  | "status":400,"error":"Bad Request" | Hbase::Table - cities |
      | missingCounty.json   | /home/discover/refresh_table.sh | DEV        | jenkins | :8087/cities/county        | :8087/cities  | "status":400,"error":"Bad Request" | Hbase::Table - cities |
      | missingAbb.json      | /home/discover/refresh_table.sh | DEV        | jenkins | :8087/cities/abbreviation  | :8087/cities  | "status":400,"error":"Bad Request" | Hbase::Table - cities |
      | incorrectFormat.json | /home/discover/refresh_table.sh | DEV        | jenkins | :8087/cities               | :8087/cities  | "status":400,"error":"Bad Request" | Hbase::Table - cities |

  @Sprint2
  Scenario Outline: Check data saved in HBase when using embedded Rabbit MQ
    Given I connect to Server: <environment> with User: <user>
    And I clean file <logFile> in indicated directory <directory> on server
    And I run embedded Rabbit MQ
    And I wait <milisecondsToWait> miliseconds
    And I run another connector
    And I run <command> command in console and wait for <message>
    When I start processor with embedded rabbit and with environment: <environment> and Processor running argument <processorRunningArgument>
    And I read CSV file <fileName>
    And I run <command2> command in console and wait for <message2>
    When I read Hbase file <logFile> from directory <directory> on server
    Then Data in CSV file matches data saved in Hbase
    And I close embedded Rabbit MQ

    Examples:
      | environment | user    | processorRunningArgument | milisecondsToWait | directory            | logFile     | fileName    | command                         | message               | command2                                                                                                                | message2            |
      | DEV        | jenkins | --spring.rabbitmq.host=  | 90000             | /home/discover/logs/ | myFileHBase | example.csv | /home/discover/refresh_table.sh | Hbase::Table - cities | (echo "scan 'cities'" \| /usr/local/hbase/bin/hbase shell > /home/discover/logs/myFileHBase); echo "Saving in file done"| Saving in file done |
