Feature: Csv file process and read in another component

# Below tests excluded due to end of free PCF trial
#  @Sprint2
#  Scenario Outline: Read message from Rabbit MQ after MAP csv file with correct city names in PCF app
#    Given I connect to Server: <environment> with User: <user>
#    And I start processor with dedicated input csv file <fileName>, environment: <environment> and Processor running argument <processorRunningArgument>
#    Then I should get correct mapping in Rabbit MQ on server <environment>
#
#    Examples:
#      | environment | user    | fileName    | processorRunningArgument |
##      | DEV         | jenkins |example.csv| --spring.rabbitmq.host=  |
#
#  @Sprint2
#  Scenario Outline: Read message from Rabbit MQ after MAP csv file with only two letters city names in PCF app
#    Given I connect to Server: <environment> with User: <user>
#    When I start processor with dedicated input csv file <fileName>, environment: <environment> and Processor running argument <processorRunningArgument>
#    Then I should get correct mapping in Rabbit MQ on server <environment>
#
#    Examples:
#      | environment | user    | fileName           | processorRunningArgument |
##      | DEV         | jenkins |shortCityNames.csv  | --spring.rabbitmq.host=  |

  @Sprint2
  Scenario Outline: Data in Kafka should match data in CSV input
    Given I connect to Server: <environment> with User: <user>
    Given I clean file <logFile> in indicated directory <directory> on server
    When I start processor with environment: <environment> and Processor running argument <processorRunningArgument>
    And I wait <milisecondsToWait> miliseconds
    And I read CSV file<fileName>
    And I read <logFile> file from directory <directory> on Virtual Machine
    Then Data in input file matches output file

    Examples:
      | environment | user    | processorRunningArgument | milisecondsToWait | directory            | logFile             | fileName    |
      | DEV         | jenkins | --spring.rabbitmq.host=  | 40000             | /home/discover/logs/ | TEST_TOPIC.sink.txt | example.csv |

  @Sprint2
  Scenario Outline:  Data in Connector should match data in CSV input
    Given I connect to Server: <environment> with User: <user>
    And I clean file <logFile> in indicated directory <directory> on server
    When I start processor with environment: <environment> and Processor running argument <processorRunningArgument>
    And I wait <milisecondsToWait> miliseconds
    And I read CSV file<fileName>
    And I read <logFile> file from directory <directory> on Virtual Machine
    Then Data in input file matches output file

    Examples:
      | environment | user    | processorRunningArgument | milisecondsToWait | directory            | logFile            | fileName    |
      | DEV         | jenkins | --spring.rabbitmq.host=  | 40000             | /home/discover/logs/ | connector_sink.csv | example.csv |

  @Sprint2
  Scenario Outline: Check data saved in HBase
    Given I connect to Server: <environment> with User: <user>
    And I clean file <logFile> in indicated directory <directory> on server
    And I run <command> command in console and wait for <message>
    When I start processor with environment: <environment> and Processor running argument <processorRunningArgument>
    And I read CSV file <fileName>
    And I run <command2> command in console and wait for <message2>
    When I read Hbase file <logFile> from directory <directory> on server
    Then Data in CSV file matches data saved in Hbase
    And I close processor

    Examples:
      | environment | user    | processorRunningArgument | directory            | logFile     | fileName    | command                         | message               | command2                                                                                                                | message2            |
      | DEV         | jenkins | --spring.rabbitmq.host=  | /home/discover/logs/ | myFileHBase | example.csv | /home/discover/refresh_table.sh | Hbase::Table - cities | (echo "scan 'cities'" \| /usr/local/hbase/bin/hbase shell > /home/discover/logs/myFileHBase); echo "Saving in file done"| Saving in file done |

