Feature: Csv file reader

  Scenario Outline: Try to read when file doesn't exists

    Given File doesn't exist <fileName>
    Then I start reader with <fileName> and I should catch correct exception <message>

    Examples:
      | fileName      | message                         |
      | Scenario1.csv | Failed to initialize the reader |

  Scenario Outline: Try to read when file exists but has wrong format
    Given File exist <fileName>
    And File has format different than csv <fileName>
    And I start Reader <fileName>
    Then File has a correct data <fileName>

    Examples:
      | fileName       |
      | Scenario2.txt  |
      | Scenario2.json |
      | Scenario2.xml  |

  Scenario Outline: Try to read when file is empty
    Given File exist <fileName>
    And File has format csv <fileName>
    And File is empty <fileName>
    And I start Reader <fileName>
    Then ArrayList should be empty

    Examples:
      | fileName      |
      | Scenario4.csv |

  Scenario Outline: Try to read when file has wrong separator
    Given File exist <fileName>
    And File has format csv <fileName>
    And File is not empty <fileName>
    And File has a wrong separator <fileName>
    And I start Reader <fileName>
    Then I should see correct error message <message>

    Examples:
      | fileName      | message                |
      | Scenario5.csv | Parsing error at line: |

  Scenario Outline: Read correct data from file

    Given File exist <fileName>
    And File has format csv <fileName>
    And File is not empty <fileName>
    And File has a correct separator <fileName>
    When I start Reader <fileName>
    Then File has a correct data <fileName>

    Examples:
      | fileName      |
      | Scenario7.csv |




